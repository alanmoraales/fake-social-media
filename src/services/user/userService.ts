import { login } from "./login";

const userService = () => ({
  login,
});

export default userService();
