import http from "@axios";
import type { ILoginUser } from "@declarations/auth";
import type { IUser } from "@declarations/user";

const login = async (body: ILoginUser) =>
  await http.post<IUser>("/auth/login", body);

export { login };
