import React from "react";
import { ChakraProvider } from "@chakra-ui/react";
import { UserProvider } from "context/user";
import Routing from "./Routing";

const App = () => (
  <ChakraProvider>
    <UserProvider>
      <Routing />
    </UserProvider>
  </ChakraProvider>
);

export default App;
