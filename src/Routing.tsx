import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "pages/Home";
import Login from "pages/Login";
import PrivateRoute from "@atoms/PrivateRoute";
import routes from "@constants/routes";

const { home, login } = routes;

const Routing = () => (
  <Router>
    <Switch>
      <PrivateRoute exact path={home}>
        <Home />
      </PrivateRoute>
      <Route exact path={login}>
        <Login />
      </Route>
    </Switch>
  </Router>
);

export default Routing;
