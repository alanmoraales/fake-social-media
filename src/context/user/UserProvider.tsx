import http from "@axios";
import React, { useState, useEffect, FC } from "react";
import UserContext, { IUserContext } from "./UserContext";
import userService from "@services/user";
import type { IUser } from "@declarations/user";
import type { ILoginUser } from "@declarations/auth";

const UserProvider: FC = ({ children }) => {
  const [userToken, setUserToken] = useState<string | undefined>(undefined);
  const [user, setUser] = useState<IUser | undefined>();
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    setIsLoggedIn(Boolean(user));
  }, [user]);

  const loginUser = async (loginData: ILoginUser) => {
    try {
      const user = await userService.login(loginData);
      setUser(user);
      http.setToken(user.username);
    } catch (error) {
      console.log(error.message);
    }
  };

  const contextValue: IUserContext = {
    user,
    isLoggedIn,
    loginUser,
  };

  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  );
};

export { UserProvider };
