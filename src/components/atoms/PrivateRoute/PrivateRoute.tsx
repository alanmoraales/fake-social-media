import React, { FC } from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import { If, Then, Else } from "react-if";
import useUserContext from "context/user";
import routes from "@constants/routes";

const { login } = routes;

const PrivateRoute: FC<RouteProps> = ({ children, ...routeProps }) => {
  const { isLoggedIn } = useUserContext();

  return (
    <If condition={isLoggedIn}>
      <Then>
        <Route {...routeProps}>{children}</Route>
      </Then>
      <Else>
        <Redirect to={login} />
      </Else>
    </If>
  );
};

export default PrivateRoute;
