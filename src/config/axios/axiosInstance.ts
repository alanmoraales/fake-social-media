import axios, { AxiosError, AxiosRequestConfig } from "axios";
import environment from "@constants/environment";

const { apiUrl } = environment;

const axiosInstance = axios.create({
  baseURL: apiUrl,
});

axiosInstance.interceptors.response.use(
  (res) => res,
  (error: AxiosError) => {
    if (error.response?.data.message) {
      throw new Error(error?.response?.data.message);
    }
    throw error;
  }
);

const sessionTokenKey = "token";

axiosInstance.interceptors.request.use((req) => {
  const token = sessionStorage.getItem(sessionTokenKey);
  if (token) {
    req.headers.authorization = `Bearer ${token}`;
  }
  return req;
});

const Http = () => {
  const setToken = (token: string) => {
    sessionStorage.setItem(sessionTokenKey, token);
  };

  const post = async <T>(
    url: string,
    data: unknown,
    config?: AxiosRequestConfig
  ) => {
    const axiosResponse = await axiosInstance.post<T>(url, data, config);
    return axiosResponse.data;
  };

  return {
    post,
    setToken,
  };
};

export default Http();
