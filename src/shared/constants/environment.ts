const environment = {
  apiUrl: String(import.meta.env.VITE_FAKE_STORE_API_URL),
};

export default environment;
