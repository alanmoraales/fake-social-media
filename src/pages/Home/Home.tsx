import { Link } from "react-router-dom";
import React from "react";
import { Heading } from "@chakra-ui/react";

const Home = () => (
  <div>
    <Heading>You can only access this route when Logged in</Heading>
    <Link to="/login">login</Link>
  </div>
);

export default Home;
