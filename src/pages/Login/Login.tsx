import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Heading,
  Grid,
  FormControl,
  FormLabel,
  Input,
  Button,
  FormErrorMessage,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import useUserContext from "context/user";
import useYupValidationResolver from "hooks/common/useYupValidationResolver";
import routes from "@constants/routes";
import type { ILoginUser } from "@declarations/auth";

const loginFormSchema = yup.object({
  username: yup.string().required("Your username is required"),
  password: yup.string().required("Your password is required"),
});

const Login = () => {
  const history = useHistory();
  const { loginUser } = useUserContext();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const resolver = useYupValidationResolver(loginFormSchema);
  const {
    handleSubmit,
    register,
    formState: { errors },
    reset,
  } = useForm<ILoginUser>({
    resolver,
  });

  const onSubmit = async (values: ILoginUser) => {
    setIsSubmitting(true);
    await loginUser(values);
    setIsSubmitting(false);
    reset();
    history.push(routes.home);
  };

  return (
    <Grid minHeight="100vh" p={8} placeItems="center">
      <Grid gap={8}>
        <Heading size="md">Login</Heading>
        <Grid as="form" gap={8} onSubmit={handleSubmit(onSubmit)}>
          <Grid gap={4}>
            <FormControl isInvalid={Boolean(errors.username)}>
              <FormLabel htmlFor="username">Username</FormLabel>
              <Input id="username" {...register("username")} />
              <FormErrorMessage>{errors.username?.message}</FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={Boolean(errors.password)}>
              <FormLabel htmlFor="password">Password</FormLabel>
              <Input id="password" type="password" {...register("password")} />
              <FormErrorMessage>{errors.password?.message}</FormErrorMessage>
            </FormControl>
          </Grid>
          <Button type="submit" isLoading={isSubmitting}>
            Continue
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Login;
